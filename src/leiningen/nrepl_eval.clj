(ns leiningen.nrepl-eval
  (:refer-clojure :exclude [eval])
  (:require [clojure.tools.nrepl :as repl]))

(def ^:dynamic *nrepl-port* 7888)

(defn get-host [& [opts]]
  (or (:host opts) "localhost"))

(defn get-port [& [opts]]
  (read-string (or (:port opts) (str *nrepl-port*))))

(defn nrepl
  "Invoke command in remote nrepl; takes :host and :port options.
   Call fun with the message response seq."
  [fun command & [opts]]
  (with-open [conn (repl/connect :host (get-host opts) :port (get-port opts))]
    (fun (-> (repl/client conn Long/MAX_VALUE)
             (repl/client-session)
             (repl/message {:op :eval :code command})))))

(defn process-response
  "Incrementally print the nrepl results."
  [results]
  (let [rc (atom 0)]
    (doseq [{:keys [out err value ex]} results]
      (when ex
        (reset! rc 1))
      (binding [*print-readably* nil]
        (when out
          (print-method out *out*)
          (.flush *out*))
        (when err
          (print-method err *err*)
          (.flush *err*)))
      (when value (println value)))
    @rc))

(defn nrepl-eval
  "Eval some code in a remote nrepl"
  {:no-project-needed true}
  [project command & {:keys [host port] :or {host "localhost", port 7888} :as opts}]
  (System/exit
   (nrepl process-response
          command
          (into {} (for [[k v] opts] [(read-string k) v])))))
